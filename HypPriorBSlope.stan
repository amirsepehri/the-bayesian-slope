data {
  int<lower=0> p; // number of variables
  int<lower=0> n; // number of observations 
  vector[n] y; // response vector
  matrix[n,p] X; // response vector
  real<lower=0> a;// shape parameter for the Gamma prior on sigma
  real<lower=0> g;// scale parameter for the Gamma prior on sigma
  vector[p] b; // prior parameters for lambda
  vector[p] c; // prior parameters for lambda  
}
parameters {
  vector[p] beta; // regression coefficients
  real<lower=0> sigma; // standard deviation
  positive_ordered[p] lambda; // penalty vector in ascending order
}
transformed parameters {
    vector[n] eta;
    vector[p] AbsBeta;
    vector[p] AccLambda;
    real<lower=0> sigma2;
    eta = X * beta;
    for (i in 1:p)
    	AbsBeta[i] = fabs(beta[i]);
    sigma2 = square(sigma);
    for (i in 1:p)
    	AccLambda[p+1-i] = log(sum(lambda[i:p]));
    	//for (j in i:p)
    	//AccLambda[p+1-i] += lambda[j]
}

model {
  sigma2 ~ inv_gamma(a,g);   //target += inv_gamma_lpdf(sigma2, a, g)
  target += log(sigma);
  target += dot_product(AccLambda,c);
  target += -dot_product(lambda,b);
  target += -dot_product(lambda,sort_asc(AbsBeta))/sigma + dot_product(AccLambda,rep_vector(1,p)) - p*log(sigma);   //SLOPE_prior(beta)
  for (j in 1:n)
  target += normal_lpdf(y[j]| eta[j], sigma);
}
