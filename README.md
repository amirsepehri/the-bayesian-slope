# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains R implementation of The Bayesian SLOPE procedure based on the package STAN. The Bayesian SLOPE is introduced in arXiv:1608.08968. Please see the paper for the details of the method and the PDF manual for the details of the R functions.

Functions in this repository require the R package 'rstan'. A test example DiabetesExample.R is included.

Repo owner: Amir Sepehri   email : amir DOT sepehri90 AT gmail DOT com