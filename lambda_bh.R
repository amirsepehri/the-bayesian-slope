#' Generate the penalty sequence lambda_bh
#'
#' @param p A number
#' @param q A number
#' @return The penalty sequence \code{lambda} 
"_PACKAGE"
#> [1] "_PACKAGE"
lambda_bh <- function(p,q){
  LAMBDA = rep(0,p)
  for (i in 1:p){
    LAMBDA[p+1-i] = qnorm(1-0.5*i*q/p, 0,1)
  }
  return(LAMBDA)
}